<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php
if ($terms = get_the_terms($post->ID, 'news_cat')) {
  if (is_array($terms)) {
    foreach ($terms as $term) {
      echo '<span class="news-cat">';
      echo esc_html($term->name);
      echo '</span>';
      break;
    }
  }
}
?>
<?php the_title(); ?>
<?php echo get_post_time( 'Y/m/d' ); ?>
<?php the_content(); ?>

<?php endwhile; else: ?>
<?php endif; ?>

<div class="prev">
  <?php next_post_link('%link', '&lt; <span>前へ</span>'); ?>
</div>
<div class="back">
  <a href="<?php echo esc_url( home_url('news') ); ?>">記事一覧</a>
</div>
<div class="next">
  <?php previous_post_link('%link', '<span>次へ</span> &gt;'); ?>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
