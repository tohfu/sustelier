<?php
////////// 管理画面カスタマイズ //////////

//// 使用しないメニューを非表示 ////
function remove_admin_menus() {
  global $menu;
  unset($menu[5]);  //投稿
  unset($menu[25]); //コメント
  //unset($menu[20]); // 固定ページ
  //unset($menu[10]); // メディア
}
add_action('admin_menu', 'remove_admin_menus');


//// ビジュアルエディタ用CSSの読み込み ////
// Gutenberg (新エディタ)
function gutenberg_block_editor_styles() {
  wp_enqueue_style( 'my_block_editor_styles', get_theme_file_uri( '/editor-style_gutenberg.css' ), false, '1.0', 'all' );
}
add_action('enqueue_block_editor_assets', 'gutenberg_block_editor_styles');


//// ビジュアルエディタ用CSSのキャッシュ削除 ////
function extend_tiny_mce_before_init($mce_init){
  $mce_init['cache_suffix']='v='.time();
  return $mce_init;
}
add_filter('tiny_mce_before_init','extend_tiny_mce_before_init');


?>
