<?php
////////// タイトルの設定 //////////

//// タイトルタグの出力　////
add_theme_support( 'title-tag' );

//詳細ページやカテゴリごとにtitleタグ全体を変更する
function change_title($title) {
  $separater = '｜'; //セパレーターを設定
  $blog_name = get_bloginfo('name'); //ブログ名を取得
  //詳細ページの場合
  if(is_single()) :
    $custom_slug = get_post_type_object(get_post_type())->label;
    $title = get_the_title().$separater.$custom_slug.$separater.$blog_name;

  //固定ページの場合
  elseif(is_page()) :
    $title = get_the_title().$separater.$blog_name;

  //タクソノミーページの場合
  elseif(is_tax()) :
    $custom_slug = get_post_type_object(get_post_type())->label;
    $title = single_term_title('', false).$separater.$custom_slug.$separater.$blog_name;

  // 年別アーカイブの場合
  elseif(is_year('news')) :
    $custom_slug = get_post_type_object(get_post_type())->label;
    $year = get_the_time('Y');
    $title = $year.$separater.$custom_slug.$separater.$blog_name;

  //アーカイブページの場合
  elseif(is_archive()) :
    $title = post_type_archive_title( '', false ).$separater.$blog_name;

  endif;

  return $title;
}
add_filter('pre_get_document_title', 'change_title');
?>
