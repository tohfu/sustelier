<?php
////////// contact form7 //////////


// セレクトボックスの空の項目を置き換え
function my_wpcf7_form_elements($html) {
  return str_replace('---', '-選択してください-', $html);
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');

// Contact Form 7のJS/CSSのファイルを必要な場合のみ読み込む
function wpcf7_file_control() {
  add_filter('wpcf7_load_js', '__return_false');
  add_filter('wpcf7_load_css', '__return_false');

  if( is_page('contact') ){
    if( function_exists('wpcf7_enqueue_scripts') ) wpcf7_enqueue_scripts();
    if( function_exists('wpcf7_enqueue_styles') ) wpcf7_enqueue_styles();
  }
}
add_action('template_redirect', 'wpcf7_file_control');
