<?php
////////// カスタム投稿タイプ設定 //////////

//News
add_action('init', 'news_init');
function news_init(){
  $labels = array(
    'name' => _x('News', 'post type general name'),
    'singular_name' => _x('News', 'post type singular name'),
    'add_new' => _x('新規追加', 'news'),
    'add_new_item' => __('新しくNewsを追加する'),
    'edit_item' => __('Newsを編集'),
    'new_item' => __('新しいNews'),
    'view_item' => __('Newsを見る'),
    'search_items' => __('Newsを探す'),
    'not_found' =>  __('Newsはありません'),
    'not_found_in_trash' => __('ゴミ箱にNewsはありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'supports' => array('title','editor','thumbnail'),
    'has_archive' => true,
    'show_in_rest' => true,
  );
  register_post_type('news',$args);

  /* カスタム分類を作成 */
  register_taxonomy(
    'news_cat',
    'news',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリー',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'news'),
      'singular_label' => 'カテゴリー',
      'show_in_rest' => true,
    )
  );
}

?>
