<?php
////////// 初期設定 //////////

//// 自動整形の無効化 ////
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');
remove_filter('the_content', 'wptexturize');


//// ビジュアルエディタの余計な機能を無効化する ////
function override_mce_options( $init_array ) {
  global $allowedposttags;

  $init_array['valid_elements']          = '*[*]';
  $init_array['extended_valid_elements'] = '*[*]';
  $init_array['valid_children']          = '+a[' . implode( '|', array_keys( $allowedposttags ) ) . ']';
  $init_array['indent']                  = true;
  $init_array['wpautop']                 = false;
  $init_array['force_p_newlines']        = false;
  $init_array['verify_html'] = false;

  return $init_array;
}
add_filter('tiny_mce_before_init', 'override_mce_options');

remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head');


//// 768pxの画像自動生成を停止 ////
update_option( 'medium_large_size_w', 0 );


//// アイキャッチ画像を有効化 ////
add_theme_support( 'post-thumbnails' );


//// URL 自動補完の無効化 ////
add_filter('redirect_canonical', 'remove_redirect_guess_404_permalink', 10, 2);
function remove_redirect_guess_404_permalink($redirect_url, $requested_url) {
  if(is_404()) {
    return false;
  }
  return $redirect_url;
}


//// CSS・JSファイルの読み込み ////

// CSSファイル
function register_style() {
  wp_register_style( 'style', get_template_directory_uri()  . '/assets/css/style.css' );

  // ライブラリ
  wp_register_style( 'slick', get_template_directory_uri()  . '/assets/lib/css/slick.css' );
  wp_register_style( 'slick-theme', get_template_directory_uri()  . '/assets/lib/css/slick-theme.css' );

}

// JSファイル
function register_script() {
  // WordPress提供のjquery.jsを読み込まない
  wp_deregister_script('jquery');
  // jQueryの読み込み
  wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), '3.4.1', true);

  wp_register_script( 'script', get_template_directory_uri()  . '/assets/js/scripts.js', '', '', 'true' );
  wp_register_script( 'top', get_template_directory_uri()  . '/assets/js/top.js', '', '', 'true' );

  // ライブラリ
  wp_register_script( 'easing', get_template_directory_uri()  . '/assets/lib/js/jquery.easing.1.3.js', '', '', 'true' );
  wp_register_script( 'slick', get_template_directory_uri()  . '/assets/lib/js/slick.min.js', '', '', 'true' );

}

// ファイルの読み込み
function my_enqueue_style() {
  // 共通
  register_style();
  register_script();

  // CSS
  wp_enqueue_style( 'slick' );
  wp_enqueue_style( 'slick-theme' );
  wp_enqueue_style( 'style' );

  // JS
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'easing' );
  wp_enqueue_script( 'slick' );
  wp_enqueue_script( 'script' );

  // トップページ
  if ( is_home() ) :
    wp_enqueue_script( 'top' );
  endif;
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_style' );


// CSS・JSファイルのバージョン情報を削除
function remove_src_ver( $src ) {
  return remove_query_arg( 'ver', $src );
}
add_filter( 'script_loader_src', 'remove_src_ver' );
add_filter( 'style_loader_src', 'remove_src_ver' );

?>
