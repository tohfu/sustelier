<?php get_header(); ?>

<?php
/// NEWS ///
$args = array(
  'posts_per_page'   => 3,
  'post_type'        => 'news',
  'post_status'      => 'publish' );
$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
<?php
	// アイキャッチ画像
	$thumbnail_id = get_post_thumbnail_id();
	if($thumbnail_id){
		$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'thumbnail' );
		$eye_url = $eye_img[0];
	} else {
		$eye_url = esc_url( get_template_directory_uri() ) .'/assets/images/common/news_thumb_noimage.jpg';
	}
?>
<a href="<?php the_permalink(); ?>"></a>
<div class="news-thumb" style="background-image: url('<?php echo $eye_url; ?>')"></div>
<?php
if ($terms = get_the_terms($post->ID, 'news_cat')) {
	if (is_array($terms)) {
	  foreach ($terms as $term) {
  	  echo '<span class="news-cat">';
	    echo esc_html($term->name);
	    echo '</span>';
	    break;
	  }
  }
}
?>
<?php echo get_post_time('Y/n/j'); ?>
<?php the_title(); ?>
<?php echo mb_substr(get_the_excerpt(), 0, 44); ?>

<?php endforeach; wp_reset_postdata(); ?>

<?php get_footer(); ?>
