<?php
  //// meta情報 ////

  // ディスクリプション
  // WP管理画面内「設定」 > 「キャッチフレーズ」に入力した文章を出力
  $description = get_bloginfo( 'description' );
  // OGP画像
  $ogp_image = esc_url( get_template_directory_uri() ) . '/assets/images/common/ogp.jpg';

  // カスタム投稿の場合
  if ( is_singular() ):

    // ディスクリプション
    if ( have_posts() ): while( have_posts() ): the_post();
      //抜粋
      $description = mb_substr( get_the_excerpt(), 0, 100 );
    endwhile; endif;

    // 画像
    $str = $post->post_content;
    $searchPattern = '/<img.*?src=(["\'])(.+?)\1.*?>/i'; //投稿記事に画像があるか調べる
    if ( has_post_thumbnail() ) : //アイキャッチがある場合
      $image_id = get_post_thumbnail_id();
      $image = wp_get_attachment_image_src( $image_id, 'large');
      $ogp_image = $image[0];
    elseif ( preg_match( $searchPattern, $str, $imgurl ) ) : //アイキャッチは無いが画像がある場合
      $ogp_image = $imgurl[2];
    endif;

  endif;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="">
<meta property="og:locale" content="ja_JP">
<meta property="og:title" content="<?php echo wp_get_document_title(); ?>">
<meta property="og:description" content="<?php echo $description; ?>">
<meta property="og:site_name" content="<?php bloginfo('name'); ?>">
<meta property="og:type" content="website" />
<meta property="og:image" content="<?php echo $ogp_image; ?>">
<link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/common/favicon.ico">
<?php wp_head(); ?>

<!--[if lt IE 9]>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/lib/js/html5.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/lib/js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<?php
  if ( is_home() ) {
    $my_body_class = 'top';
  } else if ( get_post_type() === '' ) {
    $my_body_class = '';
  } else if ( is_page() ) {
    $my_body_class = $post->post_name;
  }
?>
<body<?php echo ( $my_body_class ) ? ' class="'.$my_body_class.'"' : ''; ?>>

<?php
// ヘッダー
get_template_part( 'template-parts/header/header', 'nav' );
?>