</main>
<!-- /main -->

<!-- footer -->
<footer class="footer">
  <ul class="footer__Sns">
    <li class="footer__SnsItem -twitter">
      <a href="https://twitter.com/mocmo_Inc" target="_blank"><img src="../assets/images/common/icon_twitter.png" alt="Twitter"></a>
    </li>
    <li class="footer__SnsItem -instagram">
      <a href="https://www.instagram.com/mocmo_moc/" target="_blank"><img src="../assets/images/common/icon_instagram.png" alt="Instagram"></a>
    </li>
  </ul>
  <p class="footer__Copyright"><small>© mocmo, All Rights Reserved.</small></p>
</footer>
<!-- /footer -->

<?php wp_footer(); ?>

</body>
</html>
