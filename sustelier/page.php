<?php get_header(); ?>

<article class="pageContents">

<div class="l-main">
  <div class="l-main__Body">

    <h1 class="pageHeading">
      <span class="pageHeading__En">CONTACT</span>
      <span class="pageHeading__Jp">お問い合わせ</span>
    </h1>

    <p class="c-lead -center">お問い合わせは、お電話か下記のフォームよりお問合せください。</p>

    <div class="contact__Tel">
      <div class="contact__TelBody">
        <p class="contact__TelNumber"><a href="tel:0364471201">TEL 03-6447-1201</a></p>
        <p class="contact__TelTime">平日10:00-18:00（土日・祝日休み）</p>
      </div>
    </div>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="contact__Form">
      <p class="contact__Notes"><span>（※）</span>マークは必須項目です。</p>
      <?php the_content(); ?>
    </div>
<?php endwhile; else: ?>
<?php endif; ?>

  </div>
</div>

</article>
<?php get_footer(); ?>
